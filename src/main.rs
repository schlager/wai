fn main() {
    match std::env::current_dir() {
        Ok(path) => println!("{}", path.display()),
        Err(e) => eprintln!("Couldn't read the path, cause of {e}"),
    };
    
}
